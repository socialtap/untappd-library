package untappd

import (
	"net/url"
)

// Media represents an Untappd media from a User to a Checkin, and
// contains metadata regarding the media, and the User who submitted
// the media.
type Media struct {
	// Metadata from Untappd.
	PhotoID int

	// The actual media for a Checkin.
	Photo struct {
		Sm url.URL // small size
		Md url.URL // medium
		Lg url.URL // large
		Og url.URL // original
	}
}

// MediaPhoto represents the actual links to the photos.
type MediaPhoto struct {
	Sm url.URL // small size
	Md url.URL // medium
	Lg url.URL // large
	Og url.URL // original
}

// rawMedia is the raw JSON representation of an Untappd media.  Its data is
// unmarshaled from JSON and then exported to a Media struct.
type rawMedia struct {
	PhotoID int      `json:"photo_id"`
	Photo struct {
		Sm responseURL `json:"photo_img_sm"`
		Md responseURL `json:"photo_img_md"`
		Lg responseURL `json:"photo_img_lg"`
		Og responseURL `json:"photo_img_og"`
	} `json:"photo"`
}

// export creates an exported Media from a rawMedia struct, allowing for more
// useful structures to be created for client consumption.
func (r *rawMedia) export() *Media {
	photo := MediaPhoto{
		Sm: url.URL(r.Photo.Sm),
		Md: url.URL(r.Photo.Md),
		Lg: url.URL(r.Photo.Lg),
		Og: url.URL(r.Photo.Og),
	}
	return &Media{
		PhotoID: r.PhotoID,
		Photo: photo,
	}
}
